%define pointer 0
%macro colon 2
    %ifstr %1
        %ifid %2
            %%this:
                dq pointer
                db %1, 0
                %define pointer %%this
        %else
            %error "2nd argument of colon must be an identifier"
        %endif
    %else
        %error "1st argument of colon must be a string"
    %endif
%2:
%endmacro

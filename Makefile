lib.o: lib.asm lib.inc
	nasm -f elf64 -o lib.o lib.asm

dict.o: dict.asm lib.inc dict.inc
	nasm -f elf64 -o dict.o dict.asm

main.o: main.asm words.inc
	nasm -f elf64 -o main.o main.asm

main: lib.o dict.o main.o
	ld -o program main.o lib.o dict.o

.PHONY: test clean

test: main
	python test.py
	
clean:
	rm -rf *.o main

%include "colon.inc"

section .rodata

colon "key1", first_word
db "first expression", 0

colon "key 2 9", second_word
db "Girl, I'm back in Spanish town", 0

colon "key 3", third_word
db "Ain't no trouble coming around", 0
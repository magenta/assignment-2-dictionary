from subprocess import Popen, PIPE

inputs = ["key1", "key 2 9", "key 3", "key 4", "a"*256]
expected_outputs = ["first expression", "Girl, I'm back in Spanish town", "Ain't no trouble coming around", "", "", ""]
expected_errors = ["", "", "", "no such key in the dictionary", "too long key, it must be shorter than 255"]

for i in range(len(inputs)):
    process = Popen(["./program"], 
		    stdin=PIPE, 
		    stdout=PIPE, 
		    stderr=PIPE)
    
    stdout, stderr = process.communicate(inputs[i].encode())
    output = stdout.decode().strip()
    error = stderr.decode().strip()
    
    if "enter key:" in output:
        output = output.replace("enter key:", "").strip()
    
    if output == expected_outputs[i]:
        print(f"Right stdout for {inputs[i]}: {expected_outputs[i]}")
    else:
        print(f"Wrong stdout for {inputs[i]}: {output}, expected {expected_outputs[i]}")
        
    if error == expected_errors[i]:
        print(f"Right stderr for {inputs[i]}: {expected_errors[i]}")
    else:
        print(f"Wrong stderr for {inputs}: {error}, expected {expected_errors[i]}") 
        
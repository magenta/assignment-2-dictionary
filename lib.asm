global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_copy
global print_string_err
section .text


%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60
%define STD_IN 0
%define STD_OUT 1
%define STD_ERR 2

%define SPACE_SYMBOL 0x20
%define TAB_SYMBOL 0x9
%define NEWLINE_SYMBOL 0xA

%include 'lib.inc'
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rdi, rax
    mov rax, SYS_EXIT
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rax, SYS_WRITE
    mov rdi, STD_OUT
    syscall

    xor rax, rax
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYS_WRITE
    mov rdi, STD_OUT
    mov rsi, rsp
    mov rdx, 1
    syscall

    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rax, rdi
    mov r9, 10
    mov rsi, rsp
    dec rsp
    .loop:
        xor rdx, rdx
        div r9
        add dl, '0'
        dec rsp
        mov byte[rsp], dl
        cmp byte rax, 0
        jne .loop
        mov rdi, rsp
        push rsi
        call print_string
        pop rsp
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    test rdi, rdi
    jns .positive
    jmp .negative

    .positive:
        jmp print_uint
    .negative:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
        movzx r8, byte [rdi]
        cmp r8b, [rsi]
        jne .not_equal
        cmp r8, 0
        je .equal
        cmp byte[rsi], 0
        je .equal

        inc rdi
        inc rsi
        jmp .loop
        
    .not_equal:
        xor rax, rax
        ret
    .equal:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rdi, STD_IN
    mov rax, SYS_READ
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi ; адрес
    mov r13, rsi ; размер
    xor r14, r14  ; счетчик

    .loop:
        call read_char

        cmp rax, SPACE_SYMBOL
        je .skip_or_end
        cmp rax, TAB_SYMBOL
        je .skip_or_end
        cmp rax, NEWLINE_SYMBOL
        je .skip_or_end
        cmp rax, 0
        je .end

        mov byte[r12+r14], al
        inc r14
        cmp r14, r13
        jge .error
        jmp .loop
    
    .skip_or_end:
        cmp r14, 0
        jne .end
        jmp .loop

    .error:
        xor rax, rax
        pop r14
        pop r13
        pop r12
        ret

    .end:  
        mov byte[r12+r14], 0 
        mov rdx, r14
        mov rax, r12
        pop r14
        pop r13
        pop r12
        ret

; читает всю строку с учетом что между словами могут быть пробелы
read_line:
    push r12
    push r13
    push r14
    mov r12, rdi ; адрес
    mov r13, rsi ; размер
    xor r14, r14  ; счетчик

    .loop:
        call read_char

        cmp rax, NEWLINE_SYMBOL
        je .end
        cmp rax, 0
        je .end

        mov byte[r12+r14], al
        inc r14
        cmp r14, r13
        jge .error
        jmp .loop

    .error:
        xor rax, rax
        pop r14
        pop r13
        pop r12
        ret

    .end:
        mov byte[r12+r14], 0
        mov rdx, r14
        mov rax, r12
        pop r14
        pop r13
        pop r12
        ret
    
    
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    mov r8, 10
    xor rcx, rcx
    xor r9, r9
   
    .loop:
        movzx r9, byte[rcx+rdi]
        sub r9, 48
        cmp r9, 0
        jb .end
        cmp r9, 9
        ja .end
        mul r8
        jc .error
        add rax, r9
        jc .error
        inc rcx
        jmp .loop
    .error
        xor rdx, rdx
        ret
    .end:
        mov rdx, rcx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'
    jne parse_uint
    test rax, rax
    js .error
    inc rdi
    call parse_uint
    test rax, rax
    js .error
    test rdx, rdx
    jz .error
    inc rdx
    neg rax
    ret

    .error:
        xor rax, rax
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx

    .loop:
        mov al, [rdi]
        cmp al, 0
        je .placed
        cmp rcx, rdx
        jae .not_placed
        mov [rsi + rcx], al
        inc rdi
        inc rax
        inc rcx
        jmp .loop

    .not_placed:
        xor rax, rax
        ret
    .placed:
        mov byte[rcx+rsi], 0
        ret

print_string_err:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax         	
    mov rsi, rdi           
    mov rax, SYS_WRITE
    mov rdi, STD_ERR      
    syscall
    xor rax, rax
    ret

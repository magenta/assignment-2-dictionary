section .text
global find_word

%include 'lib.inc'
%include 'dict.inc'

find_word:
    ; rdi - string, rsi - pointer
    xor rax, rax
    .loop:
        test rsi, rsi
        jz .dict_end

        push rdi
        push rsi

        add rsi, POINTER_OFFSET
        call string_equals
        
        pop rsi
        pop rdi

        test rax, rax
        jnz .found

        mov rsi, [rsi]
        test rsi, rsi
        jnz .loop

    .dict_end:
        xor rax, rax
        ret

    .found:
        mov rax, rsi
        ret
 

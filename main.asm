%include 'words.inc'
%include 'lib.inc'
%include 'dict.inc'

%define BUF_SIZE 256
extern find_word
section .rodata
    greeting_msg: db "enter key: ", 0
    too_long_key_msg: db "too long key, it must be shorter than 255", 0
    no_such_key_msg: db "no such key in the dictionary", 0   

section .bss
	input_buffer: resb BUF_SIZE

section .text
global _start
section .text
_start:
	sub rsp, BUF_SIZE
	mov rdi, greeting_msg
	call print_string

	mov rdi, input_buffer
	mov rsi, BUF_SIZE
	call read_line
	test rax, rax
	jz .length_error

	mov rdi, rax
	mov rsi, pointer
	call find_word
	test rax, rax
	jz .key_error
    jmp .print_found_word
	
    .print_found_word:
        add rax, POINTER_OFFSET
        push rax
        mov rdi, rax
        call string_length
        pop rdi
        add rax, rdi 
        inc rax
        mov rdi, rax 
        call print_string
        jmp .end

	.key_error:
		mov rdi, no_such_key_msg
		call print_string_err
		jmp .end
	

	.length_error:
		mov rdi, too_long_key_msg
		call print_string_err
		jmp .end

	.end:
		call print_newline
		add rsp, BUF_SIZE
		xor rax, rax
		call exit
